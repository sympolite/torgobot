import discord
import random
from discord.ext import commands
import metadata


class Voice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.metadata = metadata.Metadata('resources/meta.xml')
        self.metadata.get_bot()
        self.working_dir = self.metadata.working_dir

    @commands.command(
        name="torgotheme",
        description="Sanity check command for voice channels - plays the Torgo Theme.",
        brief="Plays the Torgo theme.",

    )
    async def torgotheme(self, ctx):
        if not ctx.author.voice:
            await ctx.send("ERROR: You need to be in a voice channel to use this.")
            return
        try:
            await ctx.author.voice.channel.connect()
        except Exception as e:
            print(repr(e))
        source = discord.FFmpegPCMAudio('resources/mp3/torgo.mp3', executable='avconv')
        ctx.voice_client.play(source, after=lambda e: print(e))

    @commands.command(
        name="teriyaki",
        description="Plays a random clip of Homer Simpson making a sound, and then saying "
                    "\"Honest Abe Lincoln's Teriyaki Links.\"",
        brief="Honest Abe Lincoln's Teriyaki Links!",
    )
    async def teriyaki(self, ctx):
        if not ctx.author.voice:
            await ctx.send("ERROR: You need to be in a voice channel to use this.")
            return
        try:
            await ctx.author.voice.channel.connect()
        except Exception as e:
            print(repr(e))
        chance = random.randint(1, 5)
        source = discord.FFmpegPCMAudio(f'resources/mp3/teriyaki{chance}.mp3', executable='avconv')
        ctx.voice_client.play(source, after=lambda e: print(e))
# end VoiceCog


def setup(bot):
    bot.add_cog(Voice(bot))
