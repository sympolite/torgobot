import discord
from discord.ext import commands
import textwrap
from PIL import Image, ImageDraw, ImageFont
import torgoutils
import asyncio
import os
import random
import requests
from bs4 import BeautifulSoup
import metadata


class Text(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.metadata = metadata.Metadata('resources/meta.xml')
        self.metadata.get_bot()
        self.working_dir = self.metadata.working_dir

    @commands.command(
        name="askdrpig",
        description="Creates a Dr. Pig meme that diagnoses the user with a disease. "
                    "The disease, if not given by the user, is chosen randomly."
                    "The phrase must be wrapped in double quotes",
        brief="i diagnose you with...",
        usage="\"phrase\" (phrase is optional)"
    )
    async def askdrpig(self, ctx, phrase):
        async with ctx.typing():
            if phrase:
                disease = phrase
            else:
                diseases = open("resources/text/disease_list.txt", "r")
                disease_list = diseases.readlines()
                disease = disease_list[random.randint(1, 247)]
            base = Image.open('resources/img/dr_pig.jpg').convert('RGBA')
            copy_base = base.copy()
            txt = Image.new('RGBA', (720, 720), (255, 255, 255, 0))
            fnt = ImageFont.truetype('resources/font/arial.ttf', 44)
            rend = ImageDraw.Draw(txt)
            rend.text((20, 530), "I diagnose you with " + disease, fill=(255, 255, 255, 255), font=fnt)
            out = Image.alpha_composite(copy_base, txt)
            file_name = torgoutils.make_temp_file_name('.png')
            out.save(file_name)
            await ctx.send(content=None, file=discord.File(file_name))
            await asyncio.sleep(0.5)
            os.remove(os.path.join(self.working_dir, file_name))

    @commands.command(
        name="pearlsays",
        description="Creates a meme of Pearl writing a given phrase on a chalkboard. "
                    "The phrase must be wrapped in double-quotes.",
        brief="Pearl chalkboard meme generator.",
        usage="\"phrase\""
    )
    async def pearlsays(self, ctx, phrase):
        user = ctx.author
        print("$PEARLSAYS called by " + user.name)
        async with ctx.typing():
            if not phrase:
                await ctx.send("There were no arguments given.")
                return
            phrase = phrase.upper()
            fulltext = textwrap.fill(phrase, width=20)
            base = Image.open('resources/img/pearl_says.jpg').convert('RGBA')
            top = Image.open('resources/img/pearl_says_top.png').convert('RGBA')
            copy_base = base.copy()
            copy_top = top.copy()
            # render the text
            txt = Image.new('RGBA', (500, 309), (255, 255, 255, 0))
            fnt = ImageFont.truetype('resources/font/very_simple_chalk.ttf', 36)
            rend = ImageDraw.Draw(txt)
            rend.text((193, 43), fulltext, fill=(255, 255, 255, 255), font=fnt)
            # sandwich everything together
            out_1 = Image.alpha_composite(copy_base, txt)
            out_2 = Image.alpha_composite(out_1, copy_top)
            file_name = torgoutils.make_temp_file_name('.png')
            out_2.save(file_name)
            # and send it!
            await ctx.send(content=None, file=discord.File(file_name))
            await asyncio.sleep(0.5)
            os.remove(os.path.join(self.working_dir, file_name))

    @commands.command(
        name="setmycolor",
        description="Sets the user's color when given a valid hex code (e.g. #00ff00).",
        brief="Changes username color.",
    )
    async def setmycolor(self, ctx, color):
        user = ctx.author
        server = user.guild
        async with ctx.typing():
            if color is None:
                await ctx.send("ERROR: No color was provided.")
                return
            role = discord.utils.get(server.roles, name=color)
            if role is None:
                color_int = torgoutils.convert_to_color_int(color)
                if color_int is None:
                    await ctx.send("ERROR: the hex code was not formatted correctly.")
                    return
                role = await user.create_role(server, name=color, color=discord.Color(color_int))
            await user.add_roles(role)
            await ctx.send("Your name is now colored `" + color + "`.")

    @commands.command(
        name="hello",
        description="Sanity check command for text channels.",
        brief="Text channel sanity check.",
    )
    async def hello(self, ctx):
        async with ctx.typing():
            await ctx.send("I am TORGO!")

    @commands.command(
        name="petittube",
        description="Retrieves a video with no views.",
        brief="Retrieves a video with no views.",
    )
    async def petittube(self, ctx):
        async with ctx.typing():
            r = requests.get("http://www.petittube.com/")
            data = r.text
            soup = BeautifulSoup(data, "html.parser")
            iframe = soup.find_all('iframe')[1]
            href = iframe.get('src')
            video_source = href[href.find('=')+1:len(href)]  # get video id
            url = "https://www.youtube.com/watch?v=" + video_source
            await ctx.send("For your viewing pleasure...\n\n"+url)

    @commands.command(
        name="russianroulette",
        description="When called, this function has a 1 in 6 chance of banning the user.",
        brief="1 in 6 chance of getting kicked.",
    )
    async def russianroulette(self, ctx):
        user = ctx.author
        if random.randint(1, 6) == 6:
            await ctx.send(f"***BANG!*** You, {ctx.author.display_name}, have been kicked.")
            await user.kick()
        else:
            await ctx.send(f"*Click.* You have not been kicked... **yet.**")
# end TextCog


def setup(bot):
    bot.add_cog(Text(bot))
